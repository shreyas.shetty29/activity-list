//
//  CheckListCell.swift
//  Activity List sample app
//
//  Created by Shreyas Shetty on 04/05/19.
//  Copyright © 2019 Shreyas Shetty. All rights reserved.
//

import UIKit

protocol CheckListDelegte: class {
    func checkListDidTap(index: IndexPath, status: Bool)
}
class CheckListCell: UITableViewCell {
    @IBOutlet weak var taskNumberLabel: UILabel!
    @IBOutlet weak var checkListButton: UIButton!
    @IBOutlet weak var taskLabel: UILabel!
    weak var delegate: CheckListDelegte?
    var cellIndex: IndexPath!
    //MARK: Updates UI to reflect the task status
    var isTaskFinished: Bool = false {
        didSet {
            if isTaskFinished {
                showTaskAsFinished()
                checkListButton.setImage( #imageLiteral(resourceName: "Checked"), for: .normal)
            } else {
                showTaskAsPending()
                checkListButton.setImage(#imageLiteral(resourceName: "Unchecked"), for: .normal)
            }
        }
    }

    //MARK: Handels checkList task update
    @IBAction func checkListDidTap(_ sender: Any) {
        let checkListButton = sender as! UIButton
        if checkListButton.tag == 0 {
            checkListButton.tag = 1
            isTaskFinished = true
            showTaskAsFinished()
            delegate?.checkListDidTap(index: cellIndex, status: true)
        } else {
            checkListButton.tag = 0
            isTaskFinished = false
            showTaskAsPending()
            delegate?.checkListDidTap(index: cellIndex, status: false)
        }
    }

      //MARK: Sets taskLabel to display striked string
    private func showTaskAsFinished() {
        taskLabel.attributedText = taskLabel.text?.strikeThrough()
    }

    //MARK: Resets taskLabel to display normal string
    private func showTaskAsPending() {
        taskLabel.attributedText = taskLabel.attributedText?.paragraphStyle()
    }
}

//
//  ActivityEditView.swift
//  Activity List sample app
//
//  Created by Shreyas Shetty on 04/05/19.
//  Copyright © 2019 Shreyas Shetty. All rights reserved.
//

import UIKit

protocol ActivityEditDelegate: class {
    func didTapOnEdit()
}
class ActivityEditView: UIView {
    
    @IBOutlet var contentView: UIView!
    let nibName = "ActivityEditView"
    weak var delegate: ActivityEditDelegate?

    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUpView()
    }
    
    private func setUpView() {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        contentView = nib.instantiate(withOwner: self, options: nil).first as? UIView
        addSubview(contentView)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        contentView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        contentView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
    }

    @IBAction func onEditTapped(_ sender: Any) {
        delegate?.didTapOnEdit()
    }
    
}

//
//  ActivityCell.swift
//  Activity List sample app
//
//  Created by Shreyas Shetty on 02/05/19.
//  Copyright © 2019 Shreyas Shetty. All rights reserved.
//
//

import UIKit

protocol ActivityCellDelegate: class {
    func startTimer(index: IndexPath)
    func stopTimer(index: IndexPath, withTime: Double)
    func startEdit(index: IndexPath)
}

class ActivityCell: UICollectionViewCell {
    @IBOutlet weak var timerButton: UIButton!
    @IBOutlet weak var activityname: UILabel!
    @IBOutlet weak var activityImage: UIImageView!
    @IBOutlet weak var activityType: UILabel!
    @IBOutlet weak var pendingActivityLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var activityDateLabel: UILabel!
    private var editView: ActivityEditView?
    weak var delegate: ActivityCellDelegate?
    var indexPath: IndexPath!
    var isTimerInProgress: Bool = false {
        didSet {
            updateTimerState()
        }
    }
    
    //MARK: Updates the editing state of the cell
    var isEditingActive = false {
        didSet{
            if isEditingActive {
                editView?.contentView.backgroundColor = #colorLiteral(red: 0.4156862745, green: 0.9137254902, blue: 0.6784313725, alpha: 1)
                editView?.isHidden = false
            } else {
                editView?.contentView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                editView?.isHidden = true
            }
        }
    }

    override func awakeFromNib() {
       addEditView()
    }


    //MARK: Handels the timer interaction
    @IBAction func onTimerTapped(_ sender: Any) {
        if isTimerInProgress {
            isTimerInProgress = false
            ActivityTimer.shared.resetTimer()
            delegate?.stopTimer(index: indexPath,withTime: ActivityTimer.shared.getTime())
        } else {
            isTimerInProgress = true
            delegate?.startTimer(index: indexPath)
        }
    }

    //MARK: Updates timer button states
    private func updateTimerState() {
        if isTimerInProgress {
            timerButton.setBackgroundImage(#imageLiteral(resourceName: "Timer_2"), for: .normal)
        } else {
             timerButton.setBackgroundImage(#imageLiteral(resourceName: "Timer_1"), for: .normal)
        }
    }

    //MARK: Adds editview to cell
    func addEditView() {
        editView?.removeFromSuperview()
        editView = ActivityEditView()
        editView?.isHidden = true
        editView?.delegate = self
        if let editView = self.editView {
            self.insertSubview(editView, belowSubview: self.contentView)
            editView.translatesAutoresizingMaskIntoConstraints = false
            editView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
            editView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
           editView.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: self.contentView.bounds.width/3.5).isActive = true
            editView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        }
    }

    func removeEditView() {
     editView?.removeFromSuperview()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        self.isTimerInProgress = false
        self.indexPath = nil
    }
}

extension ActivityCell: ActivityEditDelegate {
    func didTapOnEdit() {
        delegate?.startEdit(index: indexPath)
    }
}

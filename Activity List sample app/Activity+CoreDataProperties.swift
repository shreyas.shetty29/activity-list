//
//  Activity+CoreDataProperties.swift
//  Activity Tracker App
//
//  Created by Shreyas S on 03/05/19.
//  Copyright © 2019 Shreyas Shetty. All rights reserved.
//
//

import Foundation
import CoreData


extension Activity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Activity> {
        return NSFetchRequest<Activity>(entityName: "Activity")
    }

    @NSManaged public var name: String?

}

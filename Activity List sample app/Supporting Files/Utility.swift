//
//  Utility.swift
//  Activity List sample app
//
//  Created by Shreyas Shetty on 03/05/19.
//  Copyright © 2019 Shreyas Shetty. All rights reserved.
//
//

import Foundation
import UIKit

extension Date {
    func string(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}

extension String {
    func strikeThrough() -> NSMutableAttributedString {
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: self)
        attributeString.addAttribute(.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
        return attributeString
    }

}

extension NSAttributedString {
    func paragraphStyle() -> NSMutableAttributedString {
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(attributedString: self)
        attributeString.removeAttribute(.strikethroughStyle, range: NSMakeRange(0, attributeString.length))
        return attributeString
    }
}

extension Collection {
    func stringArrayToData() -> Data? {
        if self  is [String] {
        return try? JSONSerialization.data(withJSONObject: self, options: [])
        } else {
            return nil
        }
    }

    func boolArrayToData() -> Data? {
        if self  is [Bool] {
            return try? JSONSerialization.data(withJSONObject: self, options: [])
        } else {
            return nil
        }
    }
}

extension Data {
    func dataToStringArray() -> [String]? {
        return (try? JSONSerialization.jsonObject(with: self, options: [])) as? [String]
    }

    func convertToImage() -> UIImage? {
     return UIImage(data: self)
    }

    func dataToBoolArray() -> [Bool]? {
        return (try? JSONSerialization.jsonObject(with: self, options: [])) as? [Bool]
    }
}

extension UIImage {
    func convertToData() -> Data? {
        return self.pngData()
    }
}

extension Double {
    func timeString() -> String {
        let hours = Int(self) / 3600
        let minutes = Int(self) / 60 % 60
        let seconds = Int(self) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
}

//
//  ActivityListController+CollectionView.swift
//  Activity List sample app
//
//  Created by Shreyas Shetty on 05/05/19.
//  Copyright © 2019 Shreyas Shetty. All rights reserved.
//

import Foundation
import UIKit

extension ActivityEditViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width - 20, height: 190)
    }

}

//
//  ActivityEditViewController.swift
//  Activity List sample app
//
//  Created by Shreyas Shetty on 05/05/19.
//  Copyright © 2019 Shreyas Shetty. All rights reserved.
//

import UIKit
class ActivityEditViewController: UIViewController {
    @IBOutlet weak var activityTitle: UILabel!
    @IBOutlet weak var activityImage: UIImageView!
    var activity: Activity!

    override func viewDidLoad() {
        setUpActivityViews()
    }

    //MARK: Updates views with the activity data
    private func setUpActivityViews() {
        guard let activity = self.activity else { return }
        self.activityImage.image = Data(referencing:activity.activityImage!).convertToImage()
        self.activityTitle.text = activity.title
    }

    override func viewWillAppear(_ animated: Bool) {
     self.navigationController?.navigationBar.prefersLargeTitles = false
     self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.setValue(false, forKey: "hidesShadow")
    }
}

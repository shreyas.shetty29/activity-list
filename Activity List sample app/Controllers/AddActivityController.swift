//
//  AddActivityController.swift
//  Activity List sample app
//
//  Created by Shreyas Shetty on 02/05/19.
//  Copyright © 2019 Shreyas Shetty. All rights reserved.
//
//

import UIKit
import Photos
import AVFoundation
import CoreData

class AddActivityController: UIViewController {
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addNewToDoList: UIButton!
    @IBOutlet weak var setDateButton: UIButton!
    @IBOutlet weak var datePickerDoneButton: UIButton!
    @IBOutlet weak var addActivityImageButton: UIButton!
    @IBOutlet weak var taskNameTextField: UITextField!
    @IBOutlet weak var activityNameTextField: UITextField!
    @IBOutlet weak var taskNameLabel: UILabel!
    @IBOutlet weak var activityNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    let dateFormatter = DateFormatter()
    let locale = NSLocale.current
    let toolBar = UIToolbar()
    private var tasks = [String]()
    
    override func viewDidLoad() {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: ActivityTrackerConstants.taskCell)
        addDoneButton()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.prefersLargeTitles = false
    }
    
    //MARK: Presents Imagepicker to select image
    func openGallery() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            let alert  = UIAlertController(title: ActivityTrackerConstants.Warning, message: ActivityTrackerConstants.galleryPermissionError, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: ActivityTrackerConstants.ok, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: update the datamodel with new task
    @objc func onDonePressed() {
         guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: ActivityTrackerConstants.entityName, in: managedContext)!
        let activityTitle = activityNameTextField.text ?? ""
        let checkListName = taskNameTextField.text ?? ""
        let activityImge =  imageView.image?.convertToData()
        let activityList = tasks.stringArrayToData()
        let activityListstatus = Array(repeating: false, count: tasks.count).boolArrayToData()
        var time =  TimeInterval(100)
        let timeInterVal = Data(bytes: &time, count: MemoryLayout<TimeInterval>.size)
        Activity(activityTitle: activityTitle, checkListName: checkListName, activityTime: timeInterVal, startDate: datePicker.date, activityImage: activityImge!, activityList: activityList!, activityListstatus: activityListstatus!,taskStatus: getTaskStatus(), entity: entity, insertIntoManagedObjectContext: managedContext)
        do {
            try? managedContext.save()
        }
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: Adds done button to navbar
    func addDoneButton() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: ActivityTrackerConstants.done, style: .done, target: self, action: #selector(onDonePressed))
    }
    
    //MARK: Adds new task to To-do list
    @IBAction func addTask() {
        let alert = UIAlertController(title: ActivityTrackerConstants.todoList, message: ActivityTrackerConstants.addNewItemToToDoList, preferredStyle: .alert)
        let saveAction = UIAlertAction(title: ActivityTrackerConstants.save ,style: .default) {
            [unowned self] action in
            guard let textField = alert.textFields?.first,
                let nameToSave = textField.text else {
                    return
            }
            self.tasks.append(nameToSave)
            self.tableView.reloadData()
        }
        let cancelAction = UIAlertAction(title: ActivityTrackerConstants.cancel, style: .cancel)
        alert.addTextField()
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        present(alert, animated: true)
    }
    
    @IBAction func changeImage(_ sender: Any) {
        photoGalleryAsscessRequest()
    }
    
    @IBAction func selectDate(_ sender: Any) {
        showDatePicker()
    }
    @IBAction func onDateSelected(_ sender: Any) {
        hideDatePicker()
        updateDate()
    }
    
    
}

//MARK: Photo Gallery methods
extension AddActivityController {
// check if photos permission is granted if permission is granted presents shows the gallery else provides an alert requesting for permission
    private func photoGalleryAsscessRequest() {
        PHPhotoLibrary.requestAuthorization { [weak self] result in
            guard let self = self else { return }
            if result == .authorized {
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    self.openGallery()
                }
            } else {
                self.showAlert(targetName: ActivityTrackerConstants.photoGallery) { isSucsess in
                    if isSucsess {
                        self.openGallery()
                    }
                }
            }
        }
    }
    
    private func showAlert(targetName: String, completion: @escaping (Bool)->()) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            let alertVC = UIAlertController(title: ActivityTrackerConstants.permissionAlertTitle + targetName,
                message: ActivityTrackerConstants.provideAcesstitle + targetName,
                preferredStyle: .alert)
            alertVC.addAction(UIAlertAction(title: ActivityTrackerConstants.settings, style: .default, handler: { action in
                guard   let settingsUrl = URL(string: UIApplication.openSettingsURLString),
                    UIApplication.shared.canOpenURL(settingsUrl) else { completion(false); return }
                UIApplication.shared.open(settingsUrl, options: [:]) {
                    [weak self] _ in self?.showAlert(targetName: targetName, completion: completion)
                }
            }))
            alertVC.addAction(UIAlertAction(title: ActivityTrackerConstants.cancel, style: .cancel, handler: { _ in completion(false) }))
            UIApplication.shared.delegate?.window??.rootViewController?.present(alertVC, animated: true, completion: nil)
        }
    }
}

//MARK: UITextFieldDelegate methods
extension AddActivityController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}

//MARK: TableView datasource methods
extension AddActivityController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let person = tasks[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: ActivityTrackerConstants.taskCell, for: indexPath)
        cell.textLabel?.text = person
        return cell
    }
}

//MARK: Imagepicker delegate methods
extension AddActivityController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        guard let selectedImage = info[.editedImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        imageView.image = selectedImage
        picker.dismiss(animated: true, completion: nil)
    }
    
}

extension AddActivityController {
    //MARK: Updates view elements to make date picker prominent
    func showDatePicker() {
        self.datePicker.isHidden = false
        self.datePickerDoneButton.isHidden = false
        self.navigationController?.navigationBar.isHidden = true
        self.setDateButton.isHidden = true
        self.addNewToDoList.isHidden = true
        self.addActivityImageButton.isHidden = true
        self.imageView.isHidden = true
        self.activityNameLabel.isHidden = true
        self.activityNameTextField.isHidden = true
        self.taskNameLabel.isHidden = true
        self.taskNameTextField.isHidden = true
    }

    //MARK: Resets view elements back to normal after date is choosen
    private func hideDatePicker() {
        self.datePicker.isHidden = true
        self.datePickerDoneButton.isHidden = true
        self.navigationController?.navigationBar.isHidden = false
        self.setDateButton.isHidden = false
        self.addNewToDoList.isHidden = false
        self.addActivityImageButton.isHidden = false
        self.imageView.isHidden = false
        self.activityNameLabel.isHidden = false
        self.activityNameTextField.isHidden = false
        self.taskNameLabel.isHidden = false
        self.taskNameTextField.isHidden = false
    }

    private func updateDate() {
        let date = datePicker.date.string(format: "E,d MMM yyyy")
        self.dateLabel.text = date
        self.dateLabel.isHidden = false
    }

    private func getTaskStatus() -> String {
        let totalTasks = tasks.count
         return String("(0/\(totalTasks))")
    }
}
struct ActivityData {
    var activityTitle: String
    var activityImage: NSData
    var activityType: String
    var totalTasks: Int
    var taskCompleted: Int
    var activityDate: Date
}


//
//  ActivityListController+Activity.swift
//  Activity List sample app
//
//  Created by Shreyas Shetty on 05/05/19.
//  Copyright © 2019 Shreyas Shetty. All rights reserved.
//

import Foundation
import UIKit
extension ActivityListController : ActivityCellDelegate {

    //MARK: Handles the starttimer action by the user
    func startTimer(index: IndexPath) {
        updateTimerCellsIfNeeded(index: index)
        currentActiveActivityIndex = index
        let activity = activities[index.row]
        activity.timerInprogress = true
        DataManager.shared.saveChanges()
        ActivityTimer.shared.startTimer(withTime: activity.time, activity: activity, update: { [weak self] (time) in
            if let index = self?.currentActiveActivityIndex,
                let cell = self?.activityList.cellForItem(at: index) as? ActivityCell {
                cell.timerLabel.text = time
            }
        })
    }

    //MARK: Handles stop timer action by the user
    func stopTimer(index: IndexPath, withTime: Double) {
        let activity = activities[index.row]
        activity.time = withTime
        currentActiveActivityIndex = nil
        DataManager.shared.saveChanges()
    }

    //MARK: Handles edit action by the user
    func startEdit(index: IndexPath) {
        presentActivityEditController(index: index)
        userDidSwipeRight()
    }
    
    //MARK: Presents Activity Edit controller
    func presentActivityEditController(index: IndexPath) {
        let storyboard = UIStoryboard(name: ActivityTrackerConstants.main, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: ActivityTrackerConstants.activityEditControllerId) as? ActivityEditViewController
        controller?.activity = activities[index.row]
        self.navigationController?.pushViewController(controller!, animated: true)
    }

    //MARK: Updates Timers in the cells when users scrolls
    func updateTimerCellsIfNeeded(index: IndexPath) {
        if currentActiveActivityIndex != index && currentActiveActivityIndex != nil {
            let cell = activityList.cellForItem(at: currentActiveActivityIndex) as? ActivityCell
            cell?.isTimerInProgress = false
            let timeCompleted = ActivityTimer.shared.getTime()
            ActivityTimer.shared.resetTimer()
            stopTimer(index: currentActiveActivityIndex, withTime: timeCompleted)
        }
    }
    
}

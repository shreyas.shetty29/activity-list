//
//  ActivityDetailsController+TableView.swift
//  Activity List sample app
//
//  Created by Shreyas Shetty on 05/05/19.
//  Copyright © 2019 Shreyas Shetty. All rights reserved.
//

import Foundation
import UIKit

extension ActivityDetailsController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return taskList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ActivityTrackerConstants.checklistCellIndentifier, for: indexPath) as? CheckListCell
        cell?.taskNumberLabel.text = String(indexPath.row + 1) + "."
        cell?.taskLabel.text = taskList[indexPath.row]
        cell?.isTaskFinished = taskStatus[indexPath.row]
        cell?.cellIndex = indexPath
        cell?.delegate = self
        return cell!
    }
    
}

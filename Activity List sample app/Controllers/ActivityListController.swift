//
//  ActivityListController.swift
//  Activity List sample app
//
//  Created by Shreyas Shetty on 03/05/19.
//  Copyright © 2019 Shreyas Shetty. All rights reserved.
//
//

import UIKit


class ActivityListController: UICollectionViewController{
    
    @IBOutlet var activityList: UICollectionView!
    var activeCell: ActivityCell!
    var activities = [Activity]()
    var currentActiveActivityIndex: IndexPath!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        addGestures()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        prepareData()
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    

    //MARK: Presents user with the controe to add new Activity
    @IBAction func addActivity(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: ActivityTrackerConstants.main, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: ActivityTrackerConstants.addActivity)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    // MARK: Marks the entry of collection view Delegate and data source methods
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return activities.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ActivityTrackerConstants.activityCell, for: indexPath) as? ActivityCell
        let activity = activities[indexPath.row]
        cell?.activityImage.image = Data(referencing:activity.activityImage!).convertToImage()
        cell?.activityname.text = activity.title
        cell?.activityType.text = activity.checkListName
        cell?.pendingActivityLabel.text = activity.taskStatusIndicator
        cell?.timerLabel.text = activity.time.timeString()
        cell?.activityDateLabel.text = (activity.startDate! as Date).string(format: "E,d MMM yyyy")
        cell?.delegate = self
        cell?.indexPath = indexPath
        cell?.isTimerInProgress = activity.timerInprogress
        return cell!
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: ActivityTrackerConstants.main, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: ActivityTrackerConstants.activityDetailControllerId) as? ActivityDetailsController
        controller?.activity = activities[indexPath.row]
        self.navigationController?.pushViewController(controller!, animated: true)
       userDidSwipeRight()
    }

    //MARK: Addes Gestures to colletionview to handel the edit swipe action

    private func addGestures(){
        let swipeLeft : UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(userDidSwipeLeft(_:)))
        swipeLeft.direction = .left
        collectionView?.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(userDidSwipeRight))
        swipeRight.direction = .right
        collectionView?.addGestureRecognizer(swipeRight)
    }
    
    //MARK: Fetches data from the data store
    private func prepareData() {
        DataManager.shared.retriveData { [weak self](activityList) in
            self?.activities = activityList
            self?.activityList.reloadData()
        }
    }

    //MARK: Registers the custom cell with the collectionView
    private func registerCell() {
      activityList.register(UINib(nibName: ActivityTrackerConstants.activityCell, bundle: Bundle(for: type(of: self))), forCellWithReuseIdentifier: ActivityTrackerConstants.activityCell)
    }
}

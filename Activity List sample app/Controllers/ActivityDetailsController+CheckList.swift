//
//  ActivityDetailsController+ChecklistDelegate.swift
//  Activity List sample app
//
//  Created by Shreyas Shetty on 05/05/19.
//  Copyright © 2019 Shreyas Shetty. All rights reserved.
//

import Foundation
import UIKit

extension ActivityDetailsController : CheckListDelegte {

    //MARK: Recives checklist status update
    func checkListDidTap(index: IndexPath, status: Bool) {
        taskStatus[index.row] = status
        updateActivity()
    }

    //MARK: Update activity status in coredata
    internal func updateActivity() {
        guard  let taskStatus = self.taskStatus.boolArrayToData(),
            let taskList = self.taskList.stringArrayToData() else { return }
        updateTaskStatusView()
        activity.taskStatusIndicator = pendingTaskLabel.text
        activity.checkList = taskList as NSData
        activity.checkListStatus = taskStatus as NSData
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        do {
            try? managedContext.save()
        }
    }

    //MARK: Updates UI compontes based on activity status
    internal func updateTaskStatusView() {
        let totalTasks = taskList.count
        let completedTasks = taskStatus.filter{$0}.count
        if totalTasks > 0 {
            pendingTaskLabel.isHidden = false
            pendingTaskLabel.text = String("(\(completedTasks)/\(totalTasks))")
        } else {
            pendingTaskLabel.isHidden = true
        }
    }
}

//
//  ActivityListController+Edit.swift
//  Activity List sample app
//
//  Created by Shreyas Shetty on 05/05/19.
//  Copyright © 2019 Shreyas Shetty. All rights reserved.
//

import Foundation
import UIKit


extension ActivityListController {
    //MARK: Returns the Activity cell corresponding to the location in the collectionview
    func getCellAtPoint(_ point: CGPoint) -> ActivityCell? {
        let indexPath = collectionView?.indexPathForItem(at: point)
        var cell : ActivityCell?
        if indexPath != nil {
            cell = collectionView?.cellForItem(at: indexPath!) as? ActivityCell
        } else {
            cell = nil
        }
        return cell
    }
    
    //MARK: Handels Left swipe of the cell
    @objc func userDidSwipeLeft(_ gesture : UISwipeGestureRecognizer){
        let point = gesture.location(in: collectionView)
        let duration = animationDuration()
        if(activeCell == nil){
            activeCell = getCellAtPoint(point)
            activeCell.isEditingActive = true
            UIView.animate(withDuration: duration, animations: {
                self.activeCell.contentView.transform = CGAffineTransform(translationX: -self.activeCell.frame.width/2, y: 0)
            });
        } else {
            let cell = getCellAtPoint(point)
            if cell == nil || cell == activeCell {
                activeCell = getCellAtPoint(point)
                activeCell.isEditingActive = true
                UIView.animate(withDuration: duration, animations: {
                    self.activeCell.contentView.transform = CGAffineTransform(translationX: -self.activeCell.frame.width/2, y: 0)
                });
            } else if activeCell != cell {
                activeCell.isEditingActive = false
                cell?.isEditingActive = true
                UIView.animate(withDuration: duration, animations: {
                    self.activeCell.contentView.transform = CGAffineTransform.identity
                    self.activeCell.isEditingActive = false
                    cell!.contentView.transform = CGAffineTransform(translationX: -cell!.frame.width/2, y: 0)
                }, completion: {
                    (Void) in
                    self.activeCell = cell
                })
                
            }
        }
    }
    
    //MARK: Handels right swipe by the user
    @objc func userDidSwipeRight(){
        if(activeCell != nil){
            let duration = animationDuration()
            UIView.animate(withDuration: duration, animations: {
                self.activeCell.contentView.transform = CGAffineTransform.identity
                self.activeCell.isEditingActive = false
            }, completion: {
                (Void) in
                self.activeCell = nil
            })
        }
    }
    
    func animationDuration() -> Double {
        return 0.5
    }
    
}

//
//  ActivityDetailsController.swift
//  Activity List sample app
//
//  Created by Shreyas Shetty on 04/05/19.
//  Copyright © 2019 Shreyas Shetty. All rights reserved.
//

import UIKit

class ActivityDetailsController: UIViewController {
    
    @IBOutlet weak var checkListName: UILabel!
    @IBOutlet weak var pendingTaskLabel: UILabel!
    @IBOutlet weak var checkList: UITableView!
    var activity: Activity!
    var taskList = [String]()
    var taskStatus = [Bool]()

    override func viewDidLoad() {
        setUpTaskList()
        updateTaskListName()
        updateTaskStatusView()
    }

    //MARK: Prepares checkList data to be viewed
    private func setUpTaskList() {
        guard let activity = self.activity,
            let checkList = activity.checkList,
            let taskList = Data(referencing: checkList).dataToStringArray(),
            let statusList = activity.checkListStatus,
            let taskStatus = Data(referencing: statusList).dataToBoolArray() else { return }
        self.taskList.append(contentsOf: taskList)
        self.taskStatus.append(contentsOf: taskStatus)
    }

    //MARK: Updates checkList name
    private func updateTaskListName() {
        self.checkListName.text = activity.checkListName
    }
    
    @IBAction func onAddTaskPressed(_ sender: Any) {
        let alert = UIAlertController(title: ActivityTrackerConstants.todoList, message: ActivityTrackerConstants.addNewItemToToDoList, preferredStyle: .alert)
        let saveAction = UIAlertAction(title: ActivityTrackerConstants.save ,style: .default) {
            [unowned self] action in
            guard let textField = alert.textFields?.first,
                let nameToSave = textField.text else {
                    return
            }
            self.taskList.append(nameToSave)
            self.taskStatus.append(false)
            self.checkList.reloadData()
            self.updateActivity()
        }
        let cancelAction = UIAlertAction(title: ActivityTrackerConstants.cancel, style: .cancel)
        alert.addTextField()
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        present(alert, animated: true)
    }
}

//
//  DataManager.swift
//  Activity List sample app
//
//  Created by Shreyas Shetty on 02/05/19.
//  Copyright © 2019 Shreyas Shetty. All rights reserved.
//
//

import UIKit
import CoreData
///*
// This class acts as an intermidiatory between the CoreData and the view Model
//
class DataManager {
    static let shared = DataManager()
    // MARK: Save new Activity
    //This function is responsible for adding new value into the persistance storage

    func save(activity: Activity, completion: (Bool, Activity) -> ()) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: ActivityTrackerConstants.entityName, in: managedContext)!
        guard let person = NSManagedObject(entity: entity, insertInto: managedContext) as? Activity else { return }
        person.setValue(activity.title, forKeyPath: ActivityTrackerConstants.activityTitle)
        person.setValue(activity.activityImage, forKeyPath: ActivityTrackerConstants.activityImage)
        person.setValue(activity.checkListName, forKeyPath: ActivityTrackerConstants.checkListName)
        person.setValue(activity.checkList, forKeyPath: ActivityTrackerConstants.checkList)
        person.setValue(activity.startDate, forKeyPath: ActivityTrackerConstants.activityStartDate)
        person.setValue(activity.time, forKeyPath: ActivityTrackerConstants.activityTime)
       
        
        do {
            try managedContext.save()
            completion(true,person)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }

    //MARK: Retrives the data from the coredata model and sorts it
    func retriveData(completion:([Activity]) -> ()) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: ActivityTrackerConstants.entityName)
        let sortdiscriptor = NSSortDescriptor(key: ActivityTrackerConstants.activityStartDate , ascending: true)
        fetchRequest.sortDescriptors = [sortdiscriptor]
        do {
            guard let  people = try managedContext.fetch(fetchRequest) as? [Activity] else { return }
            completion(people)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }

    //MARK: Deletes the entire data present in the model
    func deleteAllData() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: ActivityTrackerConstants.entityName)
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        fetchRequest.returnsObjectsAsFaults = false
        do {
            let results = try managedContext.fetch(fetchRequest)
            for object in results {
                guard let objectData = object as? NSManagedObject else {continue}
                managedContext.delete(objectData)
               try managedContext.save()
            }
        } catch let error {
            print("Detele all data in  error :", error)
        }
    }

    //MARK: Saves changes dones to core data model
    func saveChanges() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        do {
            try? managedContext.save()
        }
    }
}


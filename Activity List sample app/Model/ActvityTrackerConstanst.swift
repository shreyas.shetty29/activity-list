//
//  ActivityTrackerConstants.swift
//  Activity List sample app
//
//  Created by Shreyas Shetty on 02/05/19.
//  Copyright © 2019 Shreyas Shetty. All rights reserved.
//
//


// Strings are placed here to handel if future requests for localisation arises

import Foundation

class ActivityTrackerConstants {
    static let entityName = "Activity"
    static let taskCell = "taskCell"
    static let Warning  = "Warning"
    static let checklistCellIndentifier = "ChecklistCell"
    static let galleryPermissionError = "You don't have permission to access photo gallery."
    static let ok = "OK"
    static let permissionAlertTitle = "Access to the"
    static let provideAcesstitle = "Please provide access to your"
    static let settings = "Settings"
    static let cancel = "Cancel"
    static let done = "Done"
    static let todoList = "To-do List"
    static let addNewItemToToDoList = "Add a new To-Do"
    static let save = "Save"
    static let photoGallery = "photo gallery"
    static let main = "Main"
    static let addActivity = "AddActivity"
    static let activityDetailControllerId = "ActivityDetails"
    static let activityEditControllerId = "ActivityEditView"
    static let activityCell = "ActivityCell"
    static let activityImage = "activityImage"
    static let checkList = "checkList"
    static let checkListName = "checkListName"
    static let activityStartDate = "startDate"
    static let activityTime = "time"
    static let activityTitle = "title"
    static let checkListStatus = "checkListStatus"
    static let taskStatusIndicator = "taskStatusIndicator"
}

//
//  Activity+CoreDataProperties.swift
//  Activity List sample app
//
//  Created by Shreyas Shetty on 05/05/19.
//  Copyright © 2019 Shreyas Shetty. All rights reserved.
//
//

import Foundation
import CoreData


extension Activity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Activity> {
        return NSFetchRequest<Activity>(entityName: "Activity")
    }

    @NSManaged public var activityImage: NSData?
    @NSManaged public var checkList: NSData?
    @NSManaged public var checkListName: String?
    @NSManaged public var checkListStatus: NSData?
    @NSManaged public var startDate: NSDate?
    @NSManaged public var taskStatusIndicator: String?
    @NSManaged public var time: Double
    @NSManaged public var title: String?
    @NSManaged public var timerInprogress: Bool

}

//
//  ActivityTimer.swift
//  Activity List sample app
//
//  Created by Shreyas Shetty on 04/05/19.
//  Copyright © 2019 Shreyas Shetty. All rights reserved.
//

import Foundation

class ActivityTimer {
    static let shared = ActivityTimer()
    private var timer = Timer()
    private var seconds: Double = 0
    private var isTimerActive = false
    private var currentActivity: Activity!

    //MARK: Creates timer and provides continues update of the timer status
    func startTimer(withTime: Double = 0, activity: Activity, update:@escaping (String) -> ()) {
        if !isTimerActive {
            isTimerActive = true
            seconds = withTime
            self.currentActivity = activity
            timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { [weak self](time) in
                    self?.seconds += 1
                    guard let time = (self?.seconds)?.timeString() else { return }
                    update(time)
            })
          
        }
    }

    //MARK: Returns the time from the current timer
    func getTime() -> Double {
        return seconds
    }

    //MARK: Returns the current activity
    func getCurrentActivity() -> Activity? {
        return currentActivity 
    }

    //MARK: Resets the timer
    func resetTimer() {
        timer.invalidate()
        isTimerActive = false
        currentActivity = nil
    }
}

//
//  Activity+CoreDataClass.swift
//  Activity List sample app
//
//  Created by Shreyas Shetty on 04/05/19.
//  Copyright © 2019 Shreyas Shetty. All rights reserved.
//
//

import Foundation
import CoreData


public class Activity: NSManagedObject {
    @discardableResult   convenience init(activityTitle: String, checkListName: String?, activityTime: Data?, startDate: Date, activityImage: Data, activityList: Data, activityListstatus: Data, taskStatus: String, entity: NSEntityDescription, insertIntoManagedObjectContext context: NSManagedObjectContext!) {
        self.init(entity: entity, insertInto: context)
        self.title = activityTitle
        self.checkList = activityList as NSData
        self.checkListName = checkListName
        self.startDate = startDate as NSDate
        self.activityImage = activityImage as NSData
        self.checkListStatus = activityListstatus as NSData
        self.taskStatusIndicator = taskStatus
        self.time = 0
        self.timerInprogress = false
    }
}
